import Storage from '../Storage';

export default class CookieStorage extends Storage {
  _ttl;
  _path;

  /**
   * Create a cookie storage instance.
   *
   * @param {Number} [cookieTtl]  Cookie time to live.
   * @param {String} [cookiePath] Cookie path.
   */
  constructor (cookieTtl = 3600 * 24 * 14 * 1000, cookiePath = '/') {
    super();

    this._path = cookiePath;
    this._ttl = cookieTtl;
  }

  _getIndex (key) {
    const cookies = document.cookie.split(';');
    let spl = [];
    for (let i = cookies.length; i-- > 0;) {
      spl = cookies[i].trim().split('=');
      if (spl[0].toLowerCase() === key.toLowerCase()) {
        return i;
      }
    }
    return -1;
  }

  /**
   * Get a single value by key from the storage.
   *
   * @param {string} key
   * @return {*} Resulting value.
   */
  fetch (key) {
    const index = this._getIndex(key);
    if (index === -1) {
      throw new Error(`Key ${key} does not exist.`);
    }

    const cookies = document.cookie.split(';');
    return this._decode(cookies[index].split('=')[1]);
  }

  /**
   * Check if a given key exists in the storage.
   *
   * @param {string} key Key to check for.
   * @return {boolean}
   * @abstract
   */
  has (key) {
    return this._getIndex(key) !== -1;
  }

  /**
   * Creates a new entry.
   * If the key already exist, an error will be thrown.
   *
   * @param {string} key   Key to insert.
   * @param {*}      value Value to insert.
   */
  insert (key, value) {
    const index = this._getIndex(key);
    if (index !== -1) {
      throw new Error(`Key ${key} already exist.`);
    }

    const date = new Date(this._ttl + Date.now());
    document.cookie = `${key}=${this._encode(value)}; expires=${date.toUTCString()}; path=${this._path}`;
  }

  /**
   * Remove a given value by key.
   * If key does not exist, an error will be thrown.
   *
   * @param {string} key Key to remove.
   */
  remove (key) {
    const index = this._getIndex(key);
    if (index === -1) {
      throw new Error(`Key ${key} does not exist.`);
    }

    document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=${this._path}`;
  }

  /**
   * Update a key with a given value.
   * If the key do not exist an error will be thrown.
   *
   * @param {string} key   Key to update.
   * @param {*}      value Value to set.
   * @throws Error if key do not exist.
   */
  update (key, value) {
    const index = this._getIndex(key);
    if (index === -1) {
      throw new Error(`Key ${key} does not exist.`);
    }

    const date = new Date(this._ttl + Date.now());
    document.cookie = `${key}=${this._encode(value)}; expires=${date.toUTCString()}; path=${this._path}`;
  }
}
