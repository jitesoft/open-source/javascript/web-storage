import Storage from '../Storage';

export default class LocalStorage extends Storage {
  /**
   * Get a single value by key from the storage.
   *
   * @param {string} key
   * @return {*} Resulting value.
   */
  fetch (key) {
    const item = localStorage.getItem(key);
    if (item === null) {
      throw new Error(`Key ${key} does not exist.`);
    }
    return this._decode(item);
  }

  /**
   * Check if a given key exists in the storage.
   *
   * @param {string} key Key to check for.
   * @return {boolean}
   */
  has (key) {
    return localStorage.getItem(key) !== null;
  }

  /**
   * Creates a new entry.
   * If the key already exist, an error will be thrown.
   *
   * @param {string} key   Key to insert.
   * @param {*}      value Value to insert.
   */
  insert (key, value) {
    if (this.has(key)) {
      throw new Error(`Key ${key} already exist.`);
    }
    localStorage.setItem(key, this._encode(value));
  }

  /**
   * Remove a given value by key.
   * If key does not exist, an error will be thrown.
   *
   * @param {string} key Key to remove.
   */
  remove (key) {
    if (!this.has(key)) {
      throw new Error(`Key ${key} does not exist.`);
    }
    localStorage.removeItem(key);
  }

  /**
   * Update a key with a given value.
   * If the key do not exist an error will be thrown.
   *
   * @param {string} key   Key to update.
   * @param {*}      value Value to set.
   * @throws Error if key do not exist.
   */
  update (key, value) {
    if (!this.has(key)) {
      throw new Error(`Key ${key} does not exist.`);
    }

    localStorage.setItem(key, this._encode(value));
  }
}
