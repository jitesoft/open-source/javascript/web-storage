import Storage from './Storage';
import CookieStorage from './Engines/CookieStorage';
import SessionStorage from './Engines/SessionStorage';
import LocalStorage from './Engines/LocalStorage';

export {
  CookieStorage,
  SessionStorage,
  LocalStorage,
  Storage
};

export default Storage;
