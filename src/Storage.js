/**
 * @abstract
 */
export default class Storage {
  /**
   * Encode a value from any to b64 encoded string.
   * This method is used to be able to store anything but strings as values in the
   * storage engines.
   *
   * @param {*} value Value to encode.
   * @return {string}
   * @protected
   */
  _encode (value) {
    return btoa(JSON.stringify(value));
  }

  /**
   * Decode a value from b64 to any.
   * This method is used to be able to store anything but strings as values in the
   * storage engines.
   *
   * @param {String} value Value to decode.
   * @return {any}
   * @protected
   */
  _decode (value) {
    return JSON.parse(atob(value));
  }

  /* istanbul ignore next */
  /**
   * Check if a given key exists in the storage.
   *
   * @param {string} key Key to check for.
   * @return {boolean}
   * @abstract
   */
  has (key) {}

  /**
   * Check if a given key exists in the storage.
   *
   * @alias has
   * @param {string} key Key to check for.
   * @return {boolean}
   */
  exists (key) {
    return this.has(key);
  }

  /* istanbul ignore next */
  /**
   * Creates a new entry.
   * If the key already exist, an error will be thrown.
   *
   * @param {string} key   Key to insert.
   * @param {*}      value Value to insert.
   * @abstract
   */
  insert (key, value) {}

  /* istanbul ignore next */
  /**
   * Remove a given value by key.
   * If key does not exist, an error will be thrown.
   *
   * @param {string} key Key to remove.
   * @abstract
   */
  remove (key) {}

  /* istanbul ignore next */
  /**
   * Update a key with a given value.
   * If the key do not exist an error will be thrown.
   *
   * @param {string} key   Key to update.
   * @param {*}      value Value to set.
   * @throws Error if key do not exist.
   * @abstract
   */
  update (key, value) {}

  /* istanbul ignore next */
  /**
   * Get a single value by key from the storage.
   *
   * @param {string} key
   * @return {*} Resulting value.
   * @abstract
   */
  fetch (key) {}
}
