/* eslint-disable */
/** @var {Object} page */
/** @var {String} */
const uri = 'http://127.0.0.1:8000';

describe('Test compiled cookie storage engine in chromium.', () => {
  beforeAll(async () => {
    await page.goto(uri);
    await page.addScriptTag({
      path: 'dist/cookie.js'
    });
    await page.addScriptTag({
      path: 'node_modules/@jitesoft/microtest/dist/index.js'
    });
    page.on('console', msg => console.log(msg));
  });

  beforeEach(async () => {
    const cookies = await page.cookies(uri);
    if (cookies.length > 0) {
      await page.deleteCookie(...cookies);
    }
  });

  test('Test so that page is the correct page.', async () => {
    await expect(page).toMatch('TEST PAGE.');
  });

  describe('Tests for has.', () => {
    test('Has returns true if cookie exist.', async () => {
      await page.setCookie({
        name: 'test',
        value: 'cookie'
      });
      const result = page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        return Promise.resolve(cookieStorage.has('test'));
      });
    });

    test('Has returns false if cookie does not exist.', async () => {
      const result = await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        return Promise.resolve(cookieStorage.has('test'));
      });
      expect(result).toBe(false);
    });
  });

  describe('Tests for exists (alias of has).', () => {
    test('Exists returns true if cookie exist.', async () => {
      await page.setCookie({
        name: 'test',
        value: 'cookie'
      });
      const result = await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        return Promise.resolve(cookieStorage.exists('test'));
      });
      expect(result).toBe(true);
    });

    test('Exists returns false if cookie does not exist.', async () => {
      const result = await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        return Promise.resolve(cookieStorage.exists('test'));
      });
      expect(result).toBe(false);
    });
  });

  describe('Tests for insert.', () => {
    test('Insert throws error if key already exist.', async () => {
      await page.setCookie({
        name: 'test',
        value: 'cookie'
      });
      await page.evaluate(async () => {
        await MicroTest.throws(() => {
          const cookieStorage = new WebStorageCookie();
          cookieStorage.insert('test', 'abc123');
        }, 'Key test already exist.');
      });
    });

    test('Insert adds a new cookie.', async () => {
      await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        cookieStorage.insert('test', 'abc');
      });

      expect((await page.cookies())[0]).toEqual({
        'domain': '127.0.0.1',
        'expires': expect.any(Number),
        'httpOnly': false,
        'name': 'test',
        'path': '/',
        'secure': false,
        'session': false,
        'size': 12,
        'value': 'ImFiYyI='
      });
    });
  });

  describe('Tests for update.', () => {
    test('Update throws error if no key exist.', async () => {
      await page.evaluate(async () => {
        await MicroTest.throws(() => {
          const cookieStorage = new WebStorageCookie();
          cookieStorage.update('test', 'abc123');
        }, 'Key test does not exist.');
      });
    });

    test('Update changes value on key.', async () => {
      await page.setCookie({name: 'test', value: '123'});
      await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        cookieStorage.update('test', 'abc');
      });

      expect((await page.cookies())[0]).toEqual({
        'domain': '127.0.0.1',
        'expires': expect.any(Number),
        'httpOnly': false,
        'name': 'test',
        'path': '/',
        'secure': false,
        'session': false,
        'size': 12,
        'value': 'ImFiYyI='
      });
    });
  });

  describe('Tests for remove.', () => {
    test('Remove throws error if no key exist.', async () => {
      await page.evaluate(async () => {
        await MicroTest.throws(() => {
          const cookieStorage = new WebStorageCookie();
          cookieStorage.remove('test');
        }, 'Key test does not exist.');
      });
    });

    test('Remove removes cookie.', async () => {
      await page.setCookie({name: 'test', value: '123'});
      await page.evaluate(async () => {
        const cookieStorage = new WebStorageCookie();
        cookieStorage.remove('test');
      });

      expect(await page.cookies()).toHaveLength(0);
    });

  });
});
