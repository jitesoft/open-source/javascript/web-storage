# web-storage

[![npm (scoped)](https://img.shields.io/npm/v/@jitesoft/web-storage)](https://www.npmjs.com/package/@jitesoft/web-storage)
[![Known Vulnerabilities](https://dev.snyk.io/test/npm/@jitesoft/web-storage/badge.svg)](https://dev.snyk.io/test/npm/@jitesoft/web-storage)
[![pipeline status](https://gitlab.com/jitesoft/open-source/javascript/web-storage/badges/master/pipeline.svg)](https://gitlab.com/jitesoft/open-source/javascript/web-storage/commits/master)
[![coverage report](https://gitlab.com/jitesoft/open-source/javascript/web-storage/badges/master/coverage.svg)](https://gitlab.com/jitesoft/open-source/javascript/web-storage/commits/master)
[![npm](https://img.shields.io/npm/dt/@jitesoft/web-storage)](https://www.npmjs.com/package/@jitesoft/web-storage)
[![Back project](https://img.shields.io/badge/Open%20Collective-Tip%20the%20devs!-blue.svg)](https://opencollective.com/jitesoft-open-source)

A simple wrapper lib around multiple web storage engines, such as sessionStorage, localStorage and cookies.  
The lib encodes all values passed (json and b64) amd decoded any values fetched, so the underlying storage
will only receive strings, while the api takes and returns the value expected. 

The base class (abstract) is exposed as a default export when using the es6 classes, allowing you to implement
other engines than the ones included in this code.
