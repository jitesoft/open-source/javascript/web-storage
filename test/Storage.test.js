import Storage from '../src/Storage';
// noinspection JSClosureCompilerSyntax
const storage = new Storage();

describe('Test storage class none-abstract methods.', () => {
  describe('Test storage encode and decode.', () => {
    test('Encode and Decode object.', () => {
      const testObject = {
        key1: true,
        key2: 'string',
        key3: 123
      };

      // noinspection JSUnresolvedFunction
      const value = storage._encode(testObject);
      // noinspection JSUnresolvedFunction
      expect(storage._decode(value)).toStrictEqual(testObject);
    });

    test('Encode and Decode string.', () => {
      const testString = 'A string which should be encoded and decoded.';

      // noinspection JSUnresolvedFunction
      const value = storage._encode(testString);
      // noinspection JSUnresolvedFunction
      expect(storage._decode(value)).toStrictEqual(testString);
    });

    test('Encode and Decode number.', () => {
      const testNumber = 123123123123;

      // noinspection JSUnresolvedFunction
      const value = storage._encode(testNumber);
      // noinspection JSUnresolvedFunction
      expect(storage._decode(value)).toStrictEqual(testNumber);
    });

    test('Encode and Decode Array.', () => {
      const testArray = [
        123123123123,
        'A string which should be encoded and decoded',
        {
          key1: true,
          key2: 'string',
          key3: 123
        }
      ];

      // noinspection JSUnresolvedFunction
      const value = storage._encode(testArray);
      // noinspection JSUnresolvedFunction
      expect(storage._decode(value)).toStrictEqual(testArray);
    });
  });
});
