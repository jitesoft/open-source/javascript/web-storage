import LocalStorage from '../src/Engines/LocalStorage';
const storage = new LocalStorage();
// eslint-disable-next-line no-global-assign
window = window || { localStorage: jest.fn() };
const getItem = jest.fn();
const setItem = jest.fn();
const removeItem = jest.fn();

describe('Tests for local storage engine.', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    Object.defineProperty(window, 'localStorage', {
      value: {
        getItem: getItem.mockImplementation(() => null),
        setItem: setItem.mockImplementation(() => {}),
        removeItem: removeItem.mockImplementation(() => {})
      }
    });
  });

  describe('Tests for `has`.', () => {
    test('Test has returns false if key does not exist.', () => {
      expect(storage.has('anything')).toBe(false);
      expect(getItem).toHaveBeenCalledWith('anything');
    });

    test('Test has returns true if key does exist.', () => {
      const mock = getItem.mockImplementation((val) => ({ test: 'abc123', testagain: 'efg456' }[val]));
      expect(storage.has('test')).toBe(true);
      expect(storage.has('testagain')).toBe(true);
      expect(mock).toHaveBeenCalledTimes(2);
      expect(mock).toHaveBeenNthCalledWith(1, 'test');
      expect(mock).toHaveBeenNthCalledWith(2, 'testagain');
    });
  });

  describe('Tests for `exists` (has alias).', () => {
    test('Test exists returns false if key does not exist.', () => {
      expect(storage.exists('anything')).toBe(false);
      expect(getItem).toHaveBeenCalledWith('anything');
    });

    test('Test exists returns true if key does exist.', () => {
      const mock = getItem.mockImplementation((val) => ({ test: 'abc123', testagain: 'efg456' }[val]));
      expect(storage.exists('test')).toBe(true);
      expect(storage.exists('testagain')).toBe(true);
      expect(mock).toHaveBeenCalledTimes(2);
      expect(mock).toHaveBeenNthCalledWith(1, 'test');
      expect(mock).toHaveBeenNthCalledWith(2, 'testagain');
    });
  });

  describe('Tests for `fetch`.', () => {
    test('Fetch throws error if the key does not exist.', () => {
      expect(() => storage.fetch('test')).toThrow('Key test does not exist.');
    });

    test('Fetch returns the value if the key does exist.', () => {
      // We use a b64 encode on the values to store anything passed (even ;!).
      const test = btoa(JSON.stringify('abc123'));
      const efg = btoa(JSON.stringify('ha!'));

      const mock = getItem.mockImplementation((val) => ({ test: test, efg: efg }[val]));
      expect(storage.fetch('test')).toEqual('abc123');
      expect(storage.fetch('efg')).toEqual('ha!');
      expect(mock).toHaveBeenCalledTimes(2);
      expect(mock).toHaveBeenNthCalledWith(1, 'test');
      expect(mock).toHaveBeenNthCalledWith(2, 'efg');
    });
  });

  describe('Tests for `insert`.', () => {
    test('Insert throws error if key already exist.', () => {
      const mock = getItem.mockImplementation((val) => ({ test: 'abc123', eft: 'abc' }[val]));
      expect(() => storage.insert('eft')).toThrow('Key eft already exist.');
      expect(mock).toHaveBeenCalledWith('eft');
    });

    test('Insert calls underlying storage set.', () => {
      getItem.mockImplementation(() => null);
      storage.insert('key', 'value');
      expect(setItem).toHaveBeenCalledTimes(1);
      expect(setItem).toHaveBeenCalledWith('key', btoa(JSON.stringify('value')));
    });
  });

  describe('Tests for `remove`.', () => {
    test('Remove throws error if the key does not exist.', () => {
      expect(() => storage.remove('test')).toThrow('Key test does not exist.');
    });

    test('Remove calls underlying storage remove.', () => {
      getItem.mockImplementation(() => 'value');
      storage.remove('key');
      expect(removeItem).toHaveBeenCalledTimes(1);
      expect(removeItem).toHaveBeenCalledWith('key');
    });
  });

  describe('Tests for `update`.', () => {
    test('Update throws error if the key does not exist.', () => {
      expect(() => storage.update('test')).toThrow('Key test does not exist.');
    });

    test('Update calls underlying storage set.', () => {
      getItem.mockImplementation(() => '');
      storage.update('key', 'valu');
      expect(setItem).toHaveBeenCalledTimes(1);
      expect(setItem).toHaveBeenCalledWith('key', btoa(JSON.stringify('valu')));
    });
  });
});
