import CookieStorage from '../src/Engines/CookieStorage';
const storage = new CookieStorage();
const cookieGet = jest.fn();
const cookieSet = jest.fn();
// eslint-disable-next-line no-global-assign
document = document || {};
// To make sure that our date method returns correct data, we aught to set it to
// a faked current time.
const mockDate = new Date(1574270204855);
global.Date = class extends Date {
  constructor (...args) {
    super(...args);
    return mockDate;
  }
};

describe('Tests for cookie storage engine.', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    Object.defineProperty(document, 'cookie', {
      get: cookieGet.mockImplementation(() => ''),
      set: cookieSet.mockImplementation(() => {})
    });
  });

  describe('Tests for `has`.', () => {
    test('Test has returns false if key does not exist.', () => {
      cookieGet.mockImplementation(() => '');
      expect(storage.has('anything')).toBe(false);
    });

    test('Test has returns true if key does exist.', () => {
      cookieGet.mockImplementation(() => 'test=abc123;efg=ha!;testagain=123123');
      expect(storage.has('test')).toBe(true);
      expect(storage.has('testagain')).toBe(true);
    });
  });

  describe('Tests for `exists` (has alias).', () => {
    test('Test exists returns false if key does not exist.', () => {
      cookieGet.mockImplementation(() => '');
      expect(storage.exists('anything')).toBe(false);
    });

    test('Test exists returns true if key does exist.', () => {
      cookieGet.mockImplementation(() => 'test=abc123;efg=ha!;testagain=123123');
      expect(storage.exists('test')).toBe(true);
      expect(storage.exists('testagain')).toBe(true);
    });
  });

  describe('Tests for `fetch`.', () => {
    test('Fetch throws error if the key does not exist.', () => {
      cookieGet.mockImplementation(() => '');
      expect(() => storage.fetch('test')).toThrow('Key test does not exist.');
    });

    test('Fetch returns the value if the key does exist.', () => {
      // We use a b64 encode on the values to store anything passed (even ;!).
      const test = btoa(JSON.stringify('abc123'));
      const efg = btoa(JSON.stringify('ha!'));
      cookieGet.mockImplementation(() => `test=${test};efg=${efg};testagain=123123`);
      expect(storage.fetch('test')).toEqual('abc123');
      expect(storage.fetch('efg')).toEqual('ha!');
    });
  });

  describe('Tests for `insert`.', () => {
    test('Insert throws error if key already exist.', () => {
      cookieGet.mockImplementation(() => 'test=abc;eft=ha!;saaaa=adsda;');
      expect(() => storage.insert('eft')).toThrow('Key eft already exist.');
    });

    test('Insert sends a valid cookie string on invocation.', () => {
      const expiresTime = new Date((3600 * 24 * 14) + 1574270204855);
      const mock = cookieSet.mockImplementation(() => true);
      storage.insert('test', 'abc');
      expect(mock).toHaveBeenCalledWith(`test=${btoa(JSON.stringify('abc'))}; expires=${expiresTime.toUTCString()}; path=/`);
    });
  });

  describe('Tests for `remove`.', () => {
    test('Remove throws error if the key does not exist.', () => {
      cookieGet.mockImplementation(() => '');
      expect(() => storage.remove('test')).toThrow('Key test does not exist.');
    });

    test('Remove passes a valid cookie string with date 0 as expiry on invocation.', () => {
      cookieGet.mockImplementation(() => 'test=abc;eft=ha!;saaaa=adsda;');
      const mock = cookieSet.mockImplementation(() => {});
      storage.remove('eft');
      expect(mock).toHaveBeenCalledWith('eft=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/');
    });
  });

  describe('Tests for `update`.', () => {
    test('Update throws error if the key does not exist.', () => {
      cookieGet.mockImplementation(() => 'abc=123;hej=då;');
      expect(() => storage.update('test')).toThrow('Key test does not exist.');
    });

    test('Update passes a valid cookie string on invocation.', () => {
      cookieGet.mockImplementation(() => 'test=abc;eft=ha!;saaaa=adsda;');
      const mock = cookieSet.mockImplementation(() => {});
      const value = btoa(JSON.stringify('abc123'));
      const expiresTime = new Date((3600 * 24 * 14) + 1574270204855);
      storage.update('eft', 'abc123');
      expect(mock).toHaveBeenCalledWith(`eft=${value}; expires=${expiresTime.toUTCString()}; path=/`);
    });
  });
});
