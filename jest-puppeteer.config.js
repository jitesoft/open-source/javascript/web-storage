module.exports = {
  server: {
    usedPortAction: 'kill',
    command: 'cd e2e/data && ws',
    port: 8000,
    launchTimeout: 30000,
    host: '127.0.0.1',
    protocol: 'http'
  }
};
