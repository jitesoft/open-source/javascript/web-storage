const Path = require('path');

const full = {
  mode: process.env.NODE_ENV,
  optimization: {
    minimize: process.env.NODE_ENV === 'production'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader'
      }
    ]
  },
  output: {
    filename: 'index.js',
    library: 'WebStorage',
    libraryTarget: 'umd',
    globalObject: 'window'
  },
  entry: {
    index: [
      Path.join(__dirname, 'src', 'index.js')
    ]
  }
};

const shared = {
  mode: process.env.NODE_ENV,
  optimization: {
    minimize: process.env.NODE_ENV === 'production'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader'
      }
    ]
  }
};

const cookie = Object.assign({}, shared, {
  entry: {
    cookie: [
      Path.join(__dirname, 'src', 'Engines', 'CookieStorage.js')
    ]
  },
  output: {
    filename: 'cookie.js',
    library: 'WebStorageCookie',
    libraryTarget: 'window',
    libraryExport: 'default',
    globalObject: 'window'
  }
});

const local = Object.assign({}, shared, {
  entry: {
    cookie: [
      Path.join(__dirname, 'src', 'Engines', 'LocalStorage.js')
    ]
  },
  output: {
    filename: 'local.js',
    library: 'WebStorageLocal',
    libraryTarget: 'window',
    libraryExport: 'default',
    globalObject: 'window'
  }
});

const session = Object.assign({}, shared, {
  entry: {
    cookie: [
      Path.join(__dirname, 'src', 'Engines', 'SessionStorage.js')
    ]
  },
  output: {
    filename: 'session.js',
    library: 'WebStorageSession',
    libraryTarget: 'window',
    libraryExport: 'default',
    globalObject: 'window'
  }
});

module.exports = [
  full,
  local,
  cookie,
  session
];
